
async function fetchData() {

// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

await fetch("https://jsonplaceholder.typicode.com/todos").then(res => console.log(res.json()));

// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

await fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});


// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

await fetch("https://jsonplaceholder.typicode.com/todos/1").then(res => console.log(res.json()));



// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

await fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {

	let title = json.title
    let completed = json.completed


	console.log(`The item ${title} on the list has a status of ${completed}`);

});


// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

await fetch("https://jsonplaceholder.typicode.com/todos", 
{
    method: "POST",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        title: "Created to do list item",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))


// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

await fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
    method: "PUT",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        title: "Updated To Do List Item",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))


/*
Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/

await fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
    method: "PUT",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        title: "Updated To Do List Item",
        Description: "To update the my todo list with a different data structure",
        Status: "Pending",
        dataCompleted: "Pending",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))


// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

await fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
    method: "PATCH",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        title: "Updated To Do List Item using PATCH",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))


// Update a to do list item by changing the status to complete and add a date when the status was changed.

await fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
    method: "PATCH",
    headers: {
        "Content-type" : "application/json"
    },
    body: JSON.stringify({
        status: "Completed",
        dataCompleted: "02/08/23",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json))


// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

await fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
    method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json))

}

fetchData();